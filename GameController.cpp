#include "GameController.h"

GameController::GameController(sf::RenderWindow& window, int screenWidth, int screenHeight) :
    m_Window(window),
    m_ScreenWidth(screenWidth),
    m_ScreenHeight(screenHeight),
    m_GameState(GameState::Menu),
    m_GameTitleText("PONG! - The Game", m_Font),
    m_1vs1Text("1 - 1 vs 1", m_Font),
    m_1vsCPUText("2 - 1 vs CPU", m_Font),
    m_CPUvsCPUText("3 - CPU vs CPU", m_Font),
    m_Score1(0),
    m_Score2(0),
    m_Score1Text(std::to_string(m_Score1), m_Font),
    m_Score2Text(std::to_string(m_Score2), m_Font),
    m_DashedLine(sf::Lines, DASH_NUM),
    m_Ball(BALL_RADIUS, 200.f, 450.f, 10.f, 1.f, screenWidth, screenHeight),
    m_PadLeft(PAD_WIDTH, PAD_HEIGHT, PAD_WIDTH, screenHeight / 2, screenHeight, PAD_SPEED),
    m_PadRight(PAD_WIDTH, PAD_HEIGHT, screenWidth - PAD_WIDTH, screenHeight / 2, screenHeight, PAD_SPEED),
    m_PlayerInputLeft(sf::Keyboard::W, sf::Keyboard::S),
    m_PlayerInputRight(sf::Keyboard::Up, sf::Keyboard::Down),
    m_CPUInputLeft(m_PadLeft, m_Ball, screenWidth, screenHeight),
    m_CPUInputRight(m_PadRight, m_Ball, screenWidth, screenHeight),
    m_PadLeftIsPlayer(true),
    m_PadRightIsPlayer(true)
{

    if (!m_Font.loadFromFile("Resources/bit5x5.ttf"))
    {
        std::cout << "Error while loading font bit5x5!" << std::endl;
    }

    initInstructions();
    initScoreText();
    initDashedLine();

    reset(GameState::Menu);
}

void GameController::updateEvent(sf::Event event, float dt)
{
    if (m_GameState == GameState::InGame) {
        updatePlayerInput(event);

        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape)) {
            reset(GameState::Menu);
        }
    }
    else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num1)) {
        reset(GameState::InGame);

        m_PadLeftIsPlayer = true;
        m_PadRightIsPlayer = true;
    }
    else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num2)) {
        reset(GameState::InGame);

        m_PadLeftIsPlayer = true;
        m_PadRightIsPlayer = false;
    }
    else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num3)) {
        reset(GameState::InGame);

        m_PadLeftIsPlayer = false;
        m_PadRightIsPlayer = false;
    }
}

void GameController::update(float dt)
{
    if (m_GameState == GameState::Menu) {
        drawMenu();
    }
    else {
        updateCPUInput();
        updateObjects(dt);

        checkScore();
        checkCollision();

        drawGame();
    }
}

void GameController::updatePlayerInput(sf::Event& event) {
    if (m_PadLeftIsPlayer)  m_PlayerInputLeft.update(event);
    if (m_PadRightIsPlayer) m_PlayerInputRight.update(event);
}

void GameController::updateCPUInput()
{
    if (!m_PadLeftIsPlayer)  m_CPUInputLeft.update();
    if (!m_PadRightIsPlayer) m_CPUInputRight.update();
}

void GameController::updateObjects(float dt)
{
    float deltaLeft = m_PadLeftIsPlayer ? m_PlayerInputLeft.getDelta() : m_CPUInputLeft.getDelta();
    float deltaRight = m_PadRightIsPlayer ? m_PlayerInputRight.getDelta() : m_CPUInputRight.getDelta();

    m_Ball.update(dt);
    m_PadLeft.update(dt, deltaLeft);
    m_PadRight.update(dt, deltaRight);
}

void GameController::checkScore() {
    if (m_Ball.getPosition().x <= 0 || m_Ball.getPosition().x >= m_ScreenWidth)
    {
        bool left = m_Ball.getPosition().x <= 0;
        if (left) {
            m_Score2++;
        }
        else {
            m_Score1++;
        }

        updateScoreUI();

        reset(GameState::InGame);
    }
}

void GameController::updateScoreUI()
{
    m_Score1Text.setString(std::to_string(m_Score1));
    m_Score2Text.setString(std::to_string(m_Score2));
}

void GameController::checkCollision() {
    checkPadCollision();
    checkVerticalCollision();
}

void GameController::checkPadCollision() {
    if (checkIntersectionBallAndPad(m_Ball, m_PadLeft) || checkIntersectionBallAndPad(m_Ball, m_PadRight)) {
        m_Ball.horizontalCollision();
    }
}

bool GameController::checkIntersectionBallAndPad(Ball& ball, Pad& pad) const
{
    if (!pad.CanCollide()) return false;

    bool collision = ball.getShape().getGlobalBounds().intersects(pad.getShape().getGlobalBounds());

    if (collision) pad.OnCollision();

    return collision;
}

void GameController::checkVerticalCollision() {
    if (m_Ball.getPosition().y <= BALL_RADIUS || m_Ball.getPosition().y >= m_ScreenHeight - BALL_RADIUS)
    {
        m_Ball.verticalCollision();
    }
}

void GameController::drawMenu() {
    m_Window.draw(m_GameTitleText);
    m_Window.draw(m_1vs1Text);
    m_Window.draw(m_1vsCPUText);
    m_Window.draw(m_CPUvsCPUText);
}

void GameController::drawGame() {
    m_Window.draw(m_DashedLine);
    m_Window.draw(m_Score1Text);
    m_Window.draw(m_Score2Text);
    m_Window.draw(m_Ball);
    m_Window.draw(m_PadLeft);
    m_Window.draw(m_PadRight);
}

void GameController::reset(GameState gameState) {
    srand((unsigned int)time(NULL));

    m_GameState = gameState;

    if (m_GameState == GameState::Menu) {
        m_Score1 = 0;
        m_Score2 = 0;

        updateScoreUI();
    }

    m_Ball.reset(false);
    m_PadLeft.reset();
    m_PadRight.reset();
}

void GameController::setCenterOriginText(sf::Text& text)
{
    text.setOrigin(text.getLocalBounds().width / 2, text.getLocalBounds().height / 2);
}

void GameController::initInstructions() 
{
    float startPosition = 200.f;
    float deltaInstruction = 25.f;

    setCenterOriginText(m_GameTitleText);
    setCenterOriginText(m_1vs1Text);
    setCenterOriginText(m_1vsCPUText);
    setCenterOriginText(m_CPUvsCPUText);

    m_GameTitleText.setPosition(m_ScreenWidth / 2, startPosition + deltaInstruction);
    m_1vs1Text.setPosition(m_ScreenWidth / 2, startPosition + deltaInstruction * 3);
    m_1vsCPUText.setPosition(m_ScreenWidth / 2, startPosition + deltaInstruction * 4);
    m_CPUvsCPUText.setPosition(m_ScreenWidth / 2, startPosition + deltaInstruction * 5);
}

void GameController::initScoreText()
{
    setCenterOriginText(m_Score1Text);
    setCenterOriginText(m_Score2Text);
    m_Score1Text.setPosition(m_ScreenWidth / 2 - 60.f, 25.f);
    m_Score2Text.setPosition(m_ScreenWidth / 2 + 60.f, 25.f);
}

void GameController::initDashedLine()
{
    for (int i = 0; i < DASH_NUM; i++) {
        m_DashedLine[i].position = sf::Vector2f(m_ScreenWidth / 2, i * m_ScreenHeight / DASH_NUM + 15);
        m_DashedLine[i].color = sf::Color::White;
    }
}
