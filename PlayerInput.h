#pragma once
#include <SFML/Graphics.hpp>
#include "InputController.h"

class PlayerInput : public InputController
{
private:
	sf::Keyboard::Key m_MoveUpKey;
	sf::Keyboard::Key m_MoveDownKey;

public:
	PlayerInput(sf::Keyboard::Key moveUpKey, sf::Keyboard::Key moveDownKey);
	void update(sf::Event event);
};

