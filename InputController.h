#pragma once
#include <SFML/Graphics.hpp>

class InputController
{
protected:
	float m_Delta;

public:
	virtual float getDelta() const;
};
