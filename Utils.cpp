#include "Utils.h"
#include <stdlib.h>

/// <summary>
/// Generate random float between min and max (inclusive)
/// </summary>
/// <param name="min">Min value (inclusive)</param>
/// <param name="max">Max value (inclusive)</param>
/// <returns>Random int.</returns>
float Utils::random(float min, float max)
{
    float range = max - min;
    float result = ((float(rand()) / (float)RAND_MAX) * range) + min;
    return result;
}

/// <summary>
/// Generate random integer between min and max (inclusive)
/// </summary>
/// <param name="min">Min value (inclusive)</param>
/// <param name="max">Max value (inclusive)</param>
/// <returns>Random int.</returns>
int Utils::random(int min, int max)
{
    int range = max + 1 - min;
    int result = rand() % range + min;
    return result;
}
