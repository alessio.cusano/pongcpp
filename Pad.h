#pragma once

#include <SFML/Graphics.hpp>

class Pad : public sf::Drawable
{
private:
	float m_Width;
	float m_Height;

	float m_xPosition;
	float m_yPosition;

	float m_ScreenHeight;
	float m_Speed;
	float m_DisableCollisionTime;

	bool m_CollisionEnable;

	sf::Color m_Color;

	sf::RectangleShape m_Shape;

	sf::Clock m_CollisionTimer;

public:
	Pad(float width, float height, float xPosition, float yPosition, float screenHeight, float speed = 400.f, sf::Color color = sf::Color::White, float disableCollisionTime = 1.f);
	void update(float dt, float delta);
	void reset();
	const sf::Vector2f& getPosition() const;
	const sf::RectangleShape& getShape() const;
	bool CanCollide() const;
	void OnCollision();

private:
	virtual void draw(sf::RenderTarget& target, sf::RenderStates state) const;
};
