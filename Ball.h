#pragma once
#include <SFML/Graphics.hpp>

#define _USE_MATH_DEFINES
#include <math.h>
#include <algorithm>
#include "Utils.h"
#include "Pad.h"

#define ANGLE_MIN 15.f
#define ANGLE_MAX 40.f
#define DELTA_ANGLE_COLLISION 10.f

class Ball : public sf::Drawable
{
private:
	float m_Radius;
	
	float m_Speed;
	float m_StartSpeed;
	float m_MaxSpeed;
	float m_IncreaseSpeed;
	float m_DelayTime;

	float m_ScreenWidth;
	float m_ScreenHeight;

	float m_PrevAngleDeg;

	sf::Color m_Color;
	sf::CircleShape m_Shape;

	sf::Clock m_Timer;
	sf::Vector2f m_Direction;

public:
	Ball(float radius, float startSpeed, float maxSpeed, float increaseSpeed, float delayTime, float screenWidth, float screenHeight, sf::Color color = sf::Color::White);
	void update(float dt);
	void reset(bool random);
	void horizontalCollision();
	void verticalCollision();
	const sf::Vector2f& getPosition() const;
	const sf::Vector2f& getDirection() const;
	const sf::CircleShape& getShape() const;

private:
	void resetDirection();
	void move(float dt);
	void setDirectionByAngle(float angleDegree, int horizontalSide, int verticalSide);
	virtual void draw(sf::RenderTarget& target, sf::RenderStates state) const;
};