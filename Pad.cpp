#include "Pad.h"

Pad::Pad(float width, float height, float xPosition, float yPosition, float screenHeight, float speed, sf::Color color, float disableCollisionTime) :
	m_Width(width), 
	m_Height(height), 
	m_xPosition(xPosition), 
	m_yPosition(yPosition), 
	m_ScreenHeight(screenHeight), 
	m_Speed(speed),
	m_Color(color),
	m_DisableCollisionTime(disableCollisionTime),
	m_CollisionEnable(true)
{
	m_Shape.setOrigin(m_Width / 2, m_Height / 2);
	m_Shape.setSize(sf::Vector2f(m_Width, m_Height));
	m_Shape.setFillColor(m_Color);
	
	reset();
}

void Pad::update(float dt, float delta)
{
	if (!m_CollisionEnable && m_CollisionTimer.getElapsedTime().asSeconds() >= m_DisableCollisionTime) m_CollisionEnable = true;

	if (delta < 0 && m_Shape.getPosition().y <= m_Height / 2 ||
		delta > 0 && m_Shape.getPosition().y >= m_ScreenHeight - m_Height / 2)
		return;

	m_Shape.move(0, delta * dt * m_Speed);
}

void Pad::reset()
{
	m_Shape.setPosition(m_xPosition, m_yPosition);
	m_CollisionEnable = true;
}

const sf::Vector2f& Pad::getPosition() const
{
	return m_Shape.getPosition();
}

const sf::RectangleShape& Pad::getShape() const
{
	return m_Shape;
}

bool Pad::CanCollide() const
{
	return m_CollisionEnable;
}

void Pad::OnCollision()
{
	m_CollisionTimer.restart();
	m_CollisionEnable = false;
}

void Pad::draw(sf::RenderTarget& target, sf::RenderStates state) const
{
	target.draw(m_Shape, state);
}
