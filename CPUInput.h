#pragma once

#include <SFML/Graphics.hpp>
#include "InputController.h"
#include "Pad.h"
#include "Ball.h"

class CPUInput : public InputController
{
private:
	Pad& m_Pad;
	Ball& m_Ball;
	float m_ScreenWidth;
	float m_ScreenHeight;

	sf::Clock clock;

public:
	CPUInput(Pad& pad, Ball& ball, float screenWidth, float screenHeight);
	void update();
};

