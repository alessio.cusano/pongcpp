#include "PlayerInput.h"

PlayerInput::PlayerInput(sf::Keyboard::Key moveUpKey, sf::Keyboard::Key moveDownKey) :
	m_MoveUpKey(moveUpKey),
	m_MoveDownKey(moveDownKey)
{
}

void PlayerInput::update(sf::Event event)
{
	float delta = 0.f;

	if (sf::Keyboard::isKeyPressed(m_MoveUpKey) ) {
		delta = -1.f;
	}
	if (sf::Keyboard::isKeyPressed(m_MoveDownKey)) {
		delta = 1.f;
	}

	m_Delta = delta;
}
