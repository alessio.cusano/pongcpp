#pragma once

#include "Ball.h"
#include "Pad.h"
#include "PlayerInput.h"
#include "CPUInput.h"

#define DASH_NUM 20
#define BALL_RADIUS 10.f
#define PAD_WIDTH 10.f
#define PAD_HEIGHT 50.f
#define PAD_SPEED 400.f

enum class GameState { Menu, InGame };

class GameController
{
private:
	sf::RenderWindow& m_Window;
	int m_ScreenWidth;
	int m_ScreenHeight;

	GameState m_GameState;

	sf::Font m_Font;
	sf::Text m_GameTitleText;
	sf::Text m_1vs1Text;
	sf::Text m_1vsCPUText;
	sf::Text m_CPUvsCPUText;

	int m_Score1;
	int m_Score2;

	sf::Text m_Score1Text;
	sf::Text m_Score2Text;

	sf::VertexArray m_DashedLine;
	Ball m_Ball;
	Pad m_PadLeft;
	Pad m_PadRight;

	PlayerInput m_PlayerInputLeft;
	PlayerInput m_PlayerInputRight;

	CPUInput m_CPUInputLeft;
	CPUInput m_CPUInputRight;

	bool m_PadLeftIsPlayer;
	bool m_PadRightIsPlayer;

public:
	GameController(sf::RenderWindow& window, int screenWidth, int screenHeight);
	void updateEvent(sf::Event event, float dt);
	void update(float dt);

private:
	void updatePlayerInput(sf::Event& event);
	void updateCPUInput();
	void updateObjects(float dt);
	void updateScoreUI();
	void checkScore();
	void checkCollision();
	void checkPadCollision();
	bool checkIntersectionBallAndPad(Ball& ball, Pad& pad) const;
	void checkVerticalCollision();
	void drawMenu();
	void drawGame();
	void reset(GameState gameState);
	void setCenterOriginText(sf::Text& text);
	void initInstructions();
	void initScoreText();
	void initDashedLine();
};

