#include "CPUInput.h"

CPUInput::CPUInput(Pad& pad, Ball& ball, float screenWidth, float screenHeight) :
    m_Pad(pad),
    m_Ball(ball),
    m_ScreenWidth(screenWidth),
    m_ScreenHeight(screenHeight)
{
    clock.restart();
}

void CPUInput::update()
{
    m_Delta = 0.f;

    if (m_Pad.getPosition().x > m_ScreenWidth / 2 && m_Ball.getDirection().x < 0 ||
        m_Pad.getPosition().x < m_ScreenWidth / 2 && m_Ball.getDirection().x > 0) {
        return;
    }

    if (m_Pad.getPosition().y > m_Ball.getPosition().y && m_Ball.getDirection().y < 0)
        m_Delta = -1.f;
    else if (m_Pad.getPosition().y < m_Ball.getPosition().y && m_Ball.getDirection().y > 0)
        m_Delta = 1.f;
}
