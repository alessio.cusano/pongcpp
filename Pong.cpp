#include <iostream>
#include "GameController.h"

#define SCREEN_WIDTH 800
#define SCREEN_HEIGHT 600

int main(int argc, char* argv[])
{
    sf::RenderWindow window(sf::VideoMode(SCREEN_WIDTH, SCREEN_HEIGHT), "PONG! - The Game");

    sf::Clock clock;
    GameController gameController(window, SCREEN_WIDTH, SCREEN_HEIGHT);

    float dt = 0;

    while (window.isOpen())
    {
        sf::Event event;

        window.clear();

        dt = clock.restart().asSeconds();

        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                window.close();

            gameController.updateEvent(event, dt);
        }

        gameController.update(dt);

        window.display();
    }

    return 0;
}