#include "Ball.h"

Ball::Ball(float radius, float startSpeed, float maxSpeed, float increaseSpeed, float delayTime, float screenWidth, float screenHeight, sf::Color color) :
	m_Radius(radius),
	m_StartSpeed(startSpeed),
	m_MaxSpeed(maxSpeed),
	m_IncreaseSpeed(increaseSpeed),
	m_DelayTime(delayTime),
	m_ScreenWidth(screenWidth), 
	m_ScreenHeight(screenHeight), 
	m_Color(color)
{
	m_Shape.setOrigin(m_Radius, m_Radius);
	m_Shape.setRadius(m_Radius);
	m_Shape.setFillColor(m_Color);

	reset(false);
}

void Ball::update(float dt)
{
	if (m_Timer.getElapsedTime().asSeconds() < m_DelayTime) return;

	m_Speed = std::min(m_MaxSpeed, m_Speed + m_IncreaseSpeed * dt);

	move(dt);
}

void Ball::reset(bool random)
{
	float randY = (float(rand()) / float((RAND_MAX)) * (m_ScreenHeight - m_Radius)) + m_Radius / 2;
	m_Shape.setPosition(m_ScreenWidth / 2, random ? randY : m_ScreenHeight / 2);
	m_Timer.restart();

	m_Speed = m_StartSpeed;

	resetDirection();
}

void Ball::horizontalCollision()
{
	int deltaAngle = Utils::random(-DELTA_ANGLE_COLLISION, DELTA_ANGLE_COLLISION);

	int horizontalSide = m_Direction.x > 0 ? 1 : 0; // 1 represent the positive sign, while 0 the negative
	int verticalSide   = m_Direction.y > 0 ? 1 : 0; // 1 represent the positive sign, while 0 the negative
	float angleDeg = std::clamp(
							m_PrevAngleDeg + deltaAngle,
							ANGLE_MIN, 
							ANGLE_MAX
						);

	setDirectionByAngle(angleDeg, horizontalSide, verticalSide);

	m_Direction.x *= -1;
}

void Ball::verticalCollision()
{
	m_Direction.y *= -1;
}

const sf::Vector2f& Ball::getPosition() const
{
	return m_Shape.getPosition();
}

const sf::Vector2f& Ball::getDirection() const
{
	return m_Direction;
}

const sf::CircleShape& Ball::getShape() const
{
	return m_Shape;
}

void Ball::move(float dt)
{
	m_Shape.move(m_Direction * dt * m_Speed);
}

void Ball::setDirectionByAngle(float angleDegree, int horizontalSide, int verticalSide)
{
	float angleRad = angleDegree * M_PI / 180.f;

	float x = cos(angleRad) * (horizontalSide == 1 ? 1 : -1); // 1 represent the positive sign, while 0 the negative
	float y = sin(angleRad) * (verticalSide == 1 ? 1 : -1);   // 1 represent the positive sign, while 0 the negative

	m_Direction.x = x;
	m_Direction.y = y;

	m_PrevAngleDeg = angleDegree;
}

void Ball::resetDirection()
{
	int horizontalSide = Utils::random(0,1);
	int verticalSide = Utils::random(0, 1);
	float angleDeg = Utils::random(ANGLE_MIN, ANGLE_MAX);

	setDirectionByAngle(angleDeg, horizontalSide, verticalSide);
}

void Ball::draw(sf::RenderTarget& target, sf::RenderStates state) const
{
	target.draw(m_Shape, state);
}